package br.com.inmetrics.CumbucaBIblioteca.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.inmetrics.CumbucaBIblioteca.models.entities.AutorModel;

public interface AutorRepository extends JpaRepository<AutorModel, Long> {
	
}
