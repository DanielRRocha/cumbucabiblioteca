package br.com.inmetrics.CumbucaBIblioteca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CumbucaBIbliotecaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CumbucaBIbliotecaApplication.class, args);
	}

}
