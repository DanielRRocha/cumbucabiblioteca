# Cumbuca Biblioteca
Projeto inicial de um serviço de gerenciamento de uma biblioteca - Inmetrics

#### Ferramentas
- Java
- Maven
- Springboot
- JPA
- JDBC
 
#### Documentação API
##### - https://app.swaggerhub.com/apis-docs/daniel.rocha/Desafio-Backend-java/2.0